import * as React from 'react';

import {
    BrowserRouter as Router,
} from 'react-router-dom';

import App from './app/App';

const Main = () => (
    <Router>
        <App />
    </Router>
);

export default Main;