import * as React from 'react';

import { Doughnut } from 'react-chartjs-2';

interface Props {

}

interface State {

}

export default class DefaultDoughnut extends React.Component<Props, State> {

    data = {
        labels: [
            'Red',
            'Green',
            'Yellow'
        ],
        datasets: [{
            data: [300, 50, 100],
            backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
            ],
            hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
            ]
        }]
    };

    constructor(props: Props, state: State) {
        super(props);
    }

    render() {
        return (
            <div className="default-doughnut">
                <Doughnut data={this.data} />
            </div>
        );
    }
}