import * as React from 'react';
import {
    Link
} from 'react-router-dom';

import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';
import { IconButton } from 'material-ui';
import {
    Dashboard as DashboardIcon,
    Add as AddIcon,
} from 'material-ui-icons';

import './DashboardSidenav.css';

interface Props {
    isOpened: boolean;
    handleToggle: any;
}

export default class DashboardSidenav extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div className="dashboard-sidenav">
                <Drawer
                    containerStyle={{ float: 'right', top: '121px' }}
                    docked={true}
                    width={250}
                    open={this.props.isOpened}
                    openSecondary={true}
                    onRequestChange={(open) => this.setState({ open })}
                    zDepth={1}
                >
                    <Link to="/dashboard" style={{ textDecoration: 'none' }}>
                        <MenuItem onClick={this.props.handleToggle} leftIcon={<DashboardIcon />}>
                            Dashboard 1
                        </MenuItem>
                    </Link>
                    <Link to="/dashboard" style={{ textDecoration: 'none' }}>
                        <MenuItem onClick={this.props.handleToggle} leftIcon={<DashboardIcon />}>
                            Dashboard 2
                        </MenuItem>
                    </Link>
                    <div className="drawer-control">
                        <IconButton >
                            <AddIcon />
                        </IconButton>
                    </div>
                </Drawer>
            </div>
        );
    }
}
