import * as React from 'react';
import {
    Link
} from 'react-router-dom';

import {
    Home as HomeIcon,
    Dashboard as DashboardIcon,

} from 'material-ui-icons';

import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

import './DefaultSidenav.css';

interface Props {
    isOpened: boolean;
    handleToggle: any;
}

export default class DefaultSidenav extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <div className="default-sidenav">
                <Drawer
                    containerStyle={{ float: 'left', top: 64 }}
                    docked={true}
                    width={250}
                    open={this.props.isOpened}
                    onRequestChange={(open) => this.props.handleToggle}
                    zDepth={5}
                >
                    <Link to="/" style={{ textDecoration: 'none' }}>
                        <MenuItem onClick={this.props.handleToggle} leftIcon={<HomeIcon />}>
                            Home
                        </MenuItem>
                    </Link>
                    <Link to="/dashboard" style={{ textDecoration: 'none' }}>
                        <MenuItem onClick={this.props.handleToggle} leftIcon={<DashboardIcon />}>
                            Dashboard
                        </MenuItem>
                    </Link>
                </Drawer>
            </div>
        );
    }
}
