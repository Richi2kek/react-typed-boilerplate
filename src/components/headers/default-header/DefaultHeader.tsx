import * as React from 'react';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';

import {
    Menu as MenuIcon,
} from 'material-ui-icons';

import './DefaultHeader.css';

interface Props {
    handleToggle: any;
    title?: string;
}

interface State {
}

export default class DefaultHeader extends React.Component<Props> {

    constructor(props: Props, state: State) {
        super(props);
    }

    render() {
        return (
            <div className="default-layout">
                <AppBar
                    iconElementLeft={<IconButton onClick={this.props.handleToggle}><MenuIcon /></IconButton>}
                    title={this.props.title}
                />
            </div>
        );
    }
}
