import * as React from 'react';

import AppRouter from '../../app/AppRouter';
import DefaultHeader from '../headers/default-header/DefaultHeader';
import DefaultSidenav from '../sidenavs/default-sidenav/DefaultSidenav';
import DefaultFooter from '../footers/DefaultFooter';

import './DefaultLayout.css';

interface Props {
    title: string;
}

interface State {
    open: boolean;
}

export default class DefaultLayout extends React.Component<Props, State> {

    constructor(props: Props, state: State) {
        super(props);
        this.state = {
            open: false
        };
    }

    handleToggle = () => {
        const isOpened = !this.state.open;
        this.setState({
            open: isOpened
        });
    }

    render() {
        return (
            <div className="default-layout">
                <DefaultHeader
                    handleToggle={this.handleToggle}
                    title={this.props.title}
                />
                <DefaultSidenav
                    isOpened={this.state.open}
                    handleToggle={this.handleToggle}
                />

                <AppRouter />

                <DefaultFooter />
            </div>
        );
    }
}
