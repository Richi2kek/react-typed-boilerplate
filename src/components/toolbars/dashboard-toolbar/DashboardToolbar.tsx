import * as React from 'react';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import {
    Toolbar,
    ToolbarGroup,
    ToolbarSeparator,
    ToolbarTitle
} from 'material-ui/Toolbar';

import {
    ViewCarousel as ViewIcon,
    Settings as SettingsButton
} from 'material-ui-icons';

interface Props {
    isOpened: boolean;
    handleToggle: any;
}

interface State {
}
export default class DashboarToolbar extends React.Component<Props, State> {

    constructor(props: Props, state: State) {
        super(props);
    }

    render() {
        return (
            <Toolbar>
                <ToolbarGroup firstChild={true}>
                    <IconButton onClick={this.props.handleToggle}>
                        <ViewIcon />
                    </IconButton>
                </ToolbarGroup>
                <ToolbarGroup>
                    <ToolbarTitle text="Options" />
                    <FontIcon className="muidocs-icon-custom-sort" />
                    <ToolbarSeparator />
                    <IconMenu
                        iconButtonElement={
                            <IconButton touch={true}>
                                <SettingsButton />
                            </IconButton>
                        }
                    >
                        <MenuItem primaryText="Help" />
                        <MenuItem primaryText="Dashboard settings" />
                    </IconMenu>
                </ToolbarGroup>
            </Toolbar>
        );
    }
}