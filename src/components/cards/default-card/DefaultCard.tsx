import * as React from 'react';

import { Card, CardHeader, CardTitle, CardText } from 'material-ui/Card';

import './DefaultCard.css';

interface Props {
    title: string;
    subtitle?: string;
    fakeContent?: boolean;
    contentTitle?: string;
    contentSubtitle?: string;
}

interface State {
}

export default class DefaultCard extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);

    }

    render() {
        return (
            <Card className="default-card">
                <CardHeader
                    title={this.props.title}
                    subtitle={this.props.subtitle}
                />
                {
                    this.props.contentTitle ?
                        <div>
                            <CardTitle
                                title={this.props.contentTitle}
                                subtitle={this.props.contentSubtitle}
                            />
                        </div>
                        : null
                }
                {
                    this.props.fakeContent === true ?
                        <CardText>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                            Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                            Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                        </CardText>
                        : null
                }
            </Card>
        );
    }
}