import * as React from 'react';

import { Card, CardHeader, CardTitle, CardText } from 'material-ui/Card';

import DefaultLinearGraph from '../../graphs/default-linear/DefaultLinearGraph';
import DefaultDoughnut from '../../graphs/default-doughnut/DefaultDoughnut';
import './GraphCard.css';

interface Props {
    title: string;
    type: boolean;
    subtitle?: string;
    fakeContent?: boolean;
    contentTitle?: string;
    contentSubtitle?: string;
}

interface State {
}

export default class GraphCard extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);

    }

    render() {
        return (
            <Card className="default-card">
                <CardHeader
                    title={this.props.title}
                    subtitle={this.props.subtitle}
                />
                {
                    this.props.contentTitle ?
                        <div>
                            <CardTitle
                                title={this.props.contentTitle}
                                subtitle={this.props.contentSubtitle}
                            />
                        </div>
                        : null
                }
                {
                    this.props.fakeContent === true ?
                        this.props.type === true ?
                            <CardText>
                                <DefaultDoughnut />
                            </CardText>
                            :
                            <CardText>
                                <DefaultLinearGraph />
                            </CardText>
                    : null
                }
            </Card>
        );
    }
}