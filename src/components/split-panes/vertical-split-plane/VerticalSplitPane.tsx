import * as React from 'react';

import * as SplitPane from 'react-split-pane';

import './VerticalSplitPane.css';

interface Props {
    left: any;
    right: any;
}

interface State {

}

export default class VerticalSplitPane extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);

    }

    render() {
        return (
            <div className="vertical-split-pane">
                <SplitPane
                    split="vertical"
                    defaultSize="400px"
                    minSize={250}
                    maxSize={500}

                >
                    <div className="left-pane">
                        {this.props.left}
                    </div>
                    <div className="left-pane">
                        {this.props.right}
                    </div>
                </SplitPane>
            </div>
        );
    }
}