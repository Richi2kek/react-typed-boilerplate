import * as React from 'react';

import * as SplitPane from 'react-split-pane';

// import VerticalSplitPane from '../vertical-split-plane/VerticalSplitPane';

import './FourSplitPane.css';

interface Props {
    first: any;
    second: any;
    third: any;
    fourth: any;
}

interface State {

}

export default class FourSplitPane extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);

    }

    render() {
        return (
            <div className="four-split-pane">
                <SplitPane
                    split="horizontal"
                    defaultSize="50%"
                    minSize={250}
                    maxSize={700}
                >
                    <SplitPane
                        split="vertical"
                        defaultSize="50%"
                        minSize={250}
                        maxSize={700}
                    >
                        <div>
                            {this.props.first}
                        </div>
                        <div>
                            {this.props.second}
                        </div>
                    </SplitPane>
                    <SplitPane
                        split="vertical"
                        defaultSize="50%"
                        minSize={250}
                        maxSize={700}
                    >
                        <div>
                            {this.props.third}
                        </div>
                        <div>
                            {this.props.fourth}
                        </div>
                    </SplitPane>
                </SplitPane>
            </div>
        );
    }
}