import * as React from 'react';

import './DefaultFooter.css';

interface Props {
}

interface State {
}

export default class DefaultFooter extends React.Component<Props> {

    constructor(props: Props, state: State) {
        super(props);
    }

    render() {
        return (
            <footer className="default-footer">
                <div>
                    Made with ♥ by <a href="https://gitlab.com/Richi2kek">Richi2kek</a>
                </div>
            </footer>
        );
    }
}
