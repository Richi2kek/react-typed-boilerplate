import * as React from 'react';

import { MuiThemeProvider } from 'material-ui/styles';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  // green100,
  // green500,
  // green700,

  blue100,
  blue500,
  blue700
} from 'material-ui/styles/colors';

import DefaultLayout from '../components/layouts/DefaultLayout';

import './App.css';

import config from '../config/app.config';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blue700,
    primary2Color: blue500,
    primary3Color: blue100,
  },
});

interface Props {

}

interface State {
  title: string;
}

class App extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      title: config.app.title
    };
  }

  render() {
    return (
      <div className="app">
        <MuiThemeProvider muiTheme={muiTheme}>
          <DefaultLayout title={this.state.title}/>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
