import * as React from 'react';
import { Card, CardHeader, CardText } from 'material-ui/Card';

import './HomePage.css';

interface Props {

}

interface State {
}

export default class DashboardPage extends React.Component <Props, State> {
    constructor(props: Props, state: State) {
        super(props);
    }
    render() {
        return (
            <div className="home-page">
                <Card>
                    <CardHeader
                        title="Home"
                        subtitle="Home sweet home..."
                    />
                    <CardText>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                        Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                        Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                    </CardText>

                </Card>
            </div>
        );
    }
}
