import * as React from 'react';

import VerticalSplitPane from '../../components/split-panes/vertical-split-plane/VerticalSplitPane';
import DefaultCard from '../../components/cards/default-card/DefaultCard';
import GraphCard from '../../components/cards/graph-card/GraphCard';

import DashboardSidenav from '../../components/sidenavs/dashboard-sidenav/DashboardSidenav';
import DashboardToolbar from '../../components/toolbars/dashboard-toolbar/DashboardToolbar';
import './DashboardPage.css';

interface Props {

}

interface State {
    open: boolean;
}

export default class DashboardPage extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);
        this.state = {
            open: false
        };
    }

    handleToggle = () => {
        const isOpened = !this.state.open;
        this.setState({
            open: isOpened
        });
    }

    render() {
        return (
            <div className="dashboard-page">
                <DashboardSidenav
                    isOpened={this.state.open}
                    handleToggle={this.handleToggle}
                />
                <DashboardToolbar isOpened={this.state.open} handleToggle={this.handleToggle} />


                <div className="dashboard-window">
                    <VerticalSplitPane
                        left={
                            <GraphCard
                                title="First card"
                                type={false}
                                subtitle="First card with graph"
                                fakeContent={true}

                            />
                        }
                        right={
                            <DefaultCard
                                title="Second card"
                                subtitle="Second card with text"
                                fakeContent={true}
                                contentTitle="Second card content title"
                                contentSubtitle="Second card content subtitle"
                            />
                        }
                    />
                </div>
                <div className="dashboard-window">
                    <VerticalSplitPane
                        left={
                            <GraphCard
                                title="First card"
                                type={true}
                                subtitle="First card with graph"
                                fakeContent={true}
                            />
                        }
                        right={
                            <DefaultCard
                                title="Second card"
                                subtitle="Second card with text"
                                fakeContent={true}
                                contentTitle="Second card content title"
                                contentSubtitle="Second card content subtitle"
                            />
                        }
                    />
                </div>
            </div>
        );
    }
}
