import * as React from 'react';
import { Card, CardHeader } from 'material-ui/Card';

import './NotFoundPage.css';

interface Props {

}

interface State {
}

export default class DashboardPage extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props);
    }
    render() {
        return (
            <div className="not-found-page">
                <Card>
                    <CardHeader
                        title="404 not found"
                        subtitle="lost ?"
                    />
                </Card>
            </div>
        );
    }
}
